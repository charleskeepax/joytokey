/* joytokey.c  --  joytokey
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2013 Charles Keepax
 */
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/extensions/XTest.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#define JS_EVENT_BUTTON         0x01    /* button pressed/released */
#define JS_EVENT_AXIS           0x02    /* joystick moved */

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

struct keydef {
    unsigned int key;
    const KeySym sym;
    const char * const str;
};

struct axis_map {
    const int min_threshold;
    struct keydef min_key;
    const int max_threshold;
    struct keydef max_key;

    unsigned int key_pressed;
};

struct js_event {
    unsigned int time;
    short value;
    unsigned char type;
    unsigned char number;
};

#include "config.h"

Display *display;

void initialise_key(struct keydef *key) {
    KeySym sym;

    if (key->str) {
        sym = XStringToKeysym(key->str);
    } else {
        sym = key->sym;
    }

    if (sym != NoSymbol) {
        key->key = XKeysymToKeycode(display, sym);
    }
}

bool initialise() {
    int i;

    for (i = 0; i < ARRAY_SIZE(axes); ++i) {
        if (axes[i].min_threshold >= axes[i].max_threshold) {
            fprintf(stderr, "Axis %d min not smaller than max\n", i);
            return false;
        }

        initialise_key(&axes[i].min_key);
        if (!axes[i].min_key.key) {
            fprintf(stderr, "Axis %d min key badly defined\n", i);
            return false;
        }
        initialise_key(&axes[i].max_key);
        if (!axes[i].max_key.key) {
            fprintf(stderr, "Axis %d max key badly defined\n", i);
            return false;
        }
    }

    for (i = 0; i < ARRAY_SIZE(buttons); ++i) {
        initialise_key(&buttons[i]);
        if (!buttons[i].key) {
            fprintf(stderr, "Button %d key badly defined\n", i);
            return false;
        }
    }

    return true;
}

void process_buttons(struct js_event* evt) {
    if (evt->number < ARRAY_SIZE(buttons)) {
        if (evt->value == 1) {
            XTestFakeKeyEvent(display, buttons[evt->number].key, True, 0);
        } else {
            XTestFakeKeyEvent(display, buttons[evt->number].key, False, 0);
        }
        XFlush(display);
    }
}

void process_axes(struct js_event* evt) {
    struct axis_map* axis;
    unsigned int pressed = 0;

    if (evt->number < ARRAY_SIZE(axes)) {
        axis = &axes[evt->number];

        if (evt->value <= axis->min_threshold) {
            pressed = axis->min_key.key;
        } else if (evt->value >= axis->max_threshold) {
            pressed = axis->max_key.key;
        } else {
            if (axis->key_pressed) {
                XTestFakeKeyEvent(display, axis->key_pressed, False, 0);
                axis->key_pressed = 0;
                XFlush(display);
            }

            return;
        }

        if (pressed != axis->key_pressed) {
            if (axis->key_pressed) {
                XTestFakeKeyEvent(display, axis->key_pressed, False, 0);
            }

            XTestFakeKeyEvent(display, pressed, True, 0);
            axis->key_pressed = pressed;
            XFlush(display);
        }
    }
}

int main() {
    struct js_event e;
    int fd;

    display = XOpenDisplay(NULL);
    if (!display) {
        fprintf(stderr, "Can't open display\n");
        exit(EXIT_FAILURE);
    }

    if (!initialise()) {
        exit(EXIT_FAILURE);
    }

    fd = open(joystick, O_RDONLY);
    if (fd == -1) {
        fprintf(stderr, "Can't open joystick\n");
        exit(EXIT_FAILURE);
    }

    while (1) {
        if (read(fd, &e, sizeof(struct js_event)) == sizeof(struct js_event)) {
            switch (e.type) {
                case JS_EVENT_AXIS:
                    process_axes(&e);
                    break;
                case JS_EVENT_BUTTON:
                    process_buttons(&e);
                    break;
                default:
                    break;
            }
        }
    }

    return 0;
}
