/* config.h  --  joytokey
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2013 Charles Keepax
 */
static const char * const joystick = "/dev/input/js0";

static struct axis_map axes[] = {
    [0] = {
        .min_threshold = -32767,
        .min_key = { .sym = XK_Left },
        .max_threshold = 32767,
        .max_key = { .sym = XK_Right },
    },
    [1] = {
        .min_threshold = -32767,
        .min_key = { .sym = XK_Up },
        .max_threshold = 32767,
        .max_key = { .sym = XK_Down },
    },
};

static struct keydef buttons[] = {
    [0] = { .str = "q" },
    [1] = { .str = "w" },
    [2] = { .str = "a" },
    [3] = { .str = "s" },
    [4] = { .str = "e" },
    [5] = { .str = "r" },
    [6] = { .str = "d" },
    [7] = { .str = "f" },
};
